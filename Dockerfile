FROM ubuntu:18.04

RUN export DEBIAN_FRONTEND=noninteractive &&\
    apt-get update &&\
    apt-get install -y -f \
    libpython2.7-minimal=2.7.15~rc1-1\
    python2.7-minimal=2.7.15~rc1-1 \
    libpython2.7-stdlib=2.7.15~rc1-1 \
    python2.7=2.7.15~rc1-1 \
    libpython2.7=2.7.15~rc1-1 \
    libpython2.7-dev=2.7.15~rc1-1 \
    python2.7-dev=2.7.15~rc1-1 \
    python-pip

RUN apt-mark hold python2.7


RUN export DEBIAN_FRONTEND=noninteractive &&\
    apt-get update &&\
    apt-get install -y \
    apache2 \
    libapache2-mod-evasive \
    libapache2-mod-jk \
    libapache2-mod-wsgi \
    libmysqlclient-dev \
    libpq-dev \
    libmemcached-dev \
    libffi-dev \
    libssl-dev \
    net-tools &&\
    apt-get clean &&\
    rm -rf /var/lib/apt/lists/*

RUN mkdir /las-conf &&\
    mkdir /virtualenvs

COPY conf/pip-reqs_1.4.txt /las-conf/pip-reqs_1.4.txt
COPY conf/pip_reqs_1.7.txt /las-conf/pip-reqs_1.7.txt

WORKDIR /
RUN pip install virtualenv
RUN pip install virtualenvwrapper
ENV WORKON_HOME /virtualenvs
RUN /bin/bash -c "source /usr/local/bin/virtualenvwrapper.sh \
    && mkvirtualenv venvdj1.4 \
    && workon venvdj1.4 \
    && pip install --default-timeout=100 -r /las-conf/pip-reqs_1.4.txt"

RUN /bin/bash -c "source /usr/local/bin/virtualenvwrapper.sh \
    && mkvirtualenv venvdj1.7 \
    && workon venvdj1.7 \
    && pip install --default-timeout=100 -r /las-conf/pip-reqs_1.7.txt"
